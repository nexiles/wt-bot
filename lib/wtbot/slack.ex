defmodule WtBot.Slack do
	use Slack

	require Logger

	# ignore funky hidden messages
	def handle_message(_message = %{hidden: true}, _slack) do
		:ok
	end

	def handle_message(message = %{type: "message"}, slack) do
		Logger.debug "handle_message: #{inspect message}"
		if Regex.run ~r/<@#{slack.me.id}>:\sping/, message.text do
			Logger.debug "*** MATCH ***"
			send_message("<@#{message.user}> pong", message.channel, slack)
		end
		:ok
	end

	# Catch all message handler so we don't crash
	def handle_message(_message, _slack) do
		# Logger.debug "handle_message: #{inspect message}"
	    :ok
	end

	def handle_connect(slack) do
		Logger.debug "handle_connect: #{inspect slack.me}"
		send_message("Yo!", "#general", slack)
		:ok
	end

	def handle_info(message, _slack) do
		Logger.debug "handle_info: #{inspect message}"
		:ok
	end

	def handle_close(reason, _slack) do
		Logger.debug "handle_close: #{inspect reason}"
		:ok
	end
end