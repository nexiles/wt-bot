defmodule WtBot.MessageConsumer do
	use GenServer
	use AMQP

	require Logger

	def start_link do
		GenServer.start_link(__MODULE__, [], [])
	end

	@exchange 		"logs"

  @routing_keys	["skynet.*", "wc002.*"]

  def init(opts) do
  	Logger.debug "MessageConsumer.init: opts #{inspect opts}"
  	rabbitmq_connect
  end

  defp rabbitmq_connect do
    case Connection.open("amqp://guest:guest@localhost") do
      {:ok, conn} ->
		  	Logger.debug "MessageConsumer.rabbitmq_connect: #{inspect conn}"
        Process.monitor(conn.pid)
        {:ok, chan} = Channel.open(conn)
        Basic.qos(chan, prefetch_count: 10)
        Exchange.declare chan, @exchange, :topic
        {:ok, %{queue: queue_name}} = Queue.declare chan, "", exclusive: true
        Logger.debug "queue: #{queue_name}"

        for binding_key <- @routing_keys do
	        Queue.bind(chan, queue_name, @exchange, routing_key: binding_key)
	      end

        {:ok, _consumer_tag} = Basic.consume(chan, queue_name, nil, no_ack: true)
        {:ok, chan}
      {:error, reason} ->
		  	Logger.debug "MessageConsumer.rabbitmq_connect: connection error: #{inspect reason}"
        # Reconnection loop
        :timer.sleep(10000)
        rabbitmq_connect
    end
	end

	def handle_info({:DOWN, _, :process, _pid, _reason}, _) do
		Logger.warn "RabbitMQ DOWN -- reconnecting ..."
	  {:ok, chan} = rabbitmq_connect
	  {:noreply, chan}
	end

  # Confirmation sent by the broker after registering this process as a consumer
  def handle_info({:basic_consume_ok, %{consumer_tag: consumer_tag}}, chan) do
    {:noreply, chan}
  end

  # Sent by the broker when the consumer is unexpectedly cancelled (such as after a queue deletion)
  def handle_info({:basic_cancel, %{consumer_tag: consumer_tag}}, chan) do
    {:stop, :normal, chan}
  end

  # Confirmation sent by the broker to the consumer process after a Basic.cancel
  def handle_info({:basic_cancel_ok, %{consumer_tag: consumer_tag}}, chan) do
    {:noreply, chan}
  end

  def handle_info({:basic_deliver, payload, meta}, chan) do
    spawn fn -> consume(chan, payload, meta) end
    {:noreply, chan}
  end

  defp consume(channel, payload, meta) do
  	Logger.debug "MessageConsumer.consume: #{inspect channel}"
    # Basic.ack channel, meta.delivery_tag
    IO.puts "RCV #{meta.routing_key}: '#{payload}'"
  end

end