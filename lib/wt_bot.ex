defmodule WtBot do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    IO.puts "Startup ..."

    slack_token = Application.get_env(:wt_bot, WtBot.Slack)[:token]
    IO.puts "api token: " <> slack_token

    # Define workers and child supervisors to be supervised
    children = [
      worker(WtBot.Slack, [slack_token]),
      worker(WtBot.MessageConsumer, [])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: WtBot.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
